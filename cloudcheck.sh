#!/bin/bash

for ip in `cat $1`
do
    check=$(whois $ip | grep NetName)
    echo $ip -- $check
done
