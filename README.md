# CloudCheck

## Description
Script that greps for the NetName property in whois result.

Why? I needed to know if a list of IPs belonged to Amazon prior to testing.

### Usage
`./cloudcheck.sh scopefile.txt`
